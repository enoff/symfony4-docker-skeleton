# Установка #

Склонировать репозиторий:
```
git clone https://bitbucket.org/enoff/symfony4-docker-skeleton.git project-name
```

Скопировать файл с переменными окружениями:
```
cp .env.dist .env
```

Настроить порты в файле .env:
```
HTTP_PORT=8080
HTTPS_PORT=8043
PMA_PORT=8081
```

Запустить docker стек:
```
docker-compose up -d
```

Вход в php-контейнер:
```
docker-compose exec php bash
```

Доступ к приложению:

http://127.0.0.1:8080/

https://127.0.0.1:8043/